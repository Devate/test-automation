import { browser } from 'protractor';
import { HomePage } from './shared/home';

const homePage = new HomePage();

describe('Going to write the First', () => {

    it('Should pass without any issues', async () => {

        await browser.get('http://www.way2automation.com/angularjs-protractor/banking/#/login/');
        await homePage.customerLoginButton.click();
        await homePage.allButtons.getText().then((text => {
            console.log('The heading is: ' + text);
        }));
    });
});
