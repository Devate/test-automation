import { element, by } from 'protractor';

// Choose Identification Type
export enum IdentificationType {
    Id,
    Name,
    Css,
    Xpath,
    LinkText,
    PartialLinkText,
    ClassName,
    ButtonText,
    Model
}

// Common Functions
export class CommonTasks {

    // Identify Web Elements
    elementLocator(obj: any) {

        switch (obj.type) {
            case IdentificationType[IdentificationType.Xpath]:
                return element(by.xpath(obj.value));

            case IdentificationType[IdentificationType.Css]:
                return element(by.css(obj.value));

            case IdentificationType[IdentificationType.Id]:
                return element(by.id(obj.value));

            case IdentificationType[IdentificationType.Name]:
                return element(by.name(obj.value));

            case IdentificationType[IdentificationType.LinkText]:
                return element(by.linkText(obj.value));

            case IdentificationType[IdentificationType.PartialLinkText]:
                return element(by.partialLinkText(obj.value));

            case IdentificationType[IdentificationType.ClassName]:
                return element(by.className(obj.value));

            case IdentificationType[IdentificationType.ButtonText]:
                return element(by.buttonText(obj.value));

            case IdentificationType[IdentificationType.Model]:
                return element(by.model(obj.value));

            default:
                return element(by.xpath(obj.value));

        }
    }
}
